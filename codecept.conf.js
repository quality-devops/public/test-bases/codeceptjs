require('dotenv').config({ path: './.env' })

exports.config = {
  output: './output',
  helpers: {
    WebDriver: {
      host: 'hub.browserstack.com',
      url: 'https://google.com',
      browser: 'chrome',
      user: process.env.BROWSERSTACK_USR, // configured in .env file
      key: process.env.BROWSERSTACK_KEY, // configured in .env file
      capabilities: {
        "browserName": "chrome",
        "browserstack.local": true,
      }
    }
  },
  bootstrap: null,
  gherkin: {
    features: './src/features/*.feature',
    steps: [
      './src/stepDefinitions/Search.js'
    ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: false
    },
    wdio: {
      enabled: true,
      services: ['selenium-standalone']
    }
  },
  tests: '../src/specs/*.spec.js',
}