
@Search
Feature: Search
    The ability to search for a text and get an expected result

    Scenario Outline: Search for text
        Given I navigate to search page
        When I search for the "<Text>"
        Then I should see results with "<Result>"

        Examples:
            |   Text    |   Result  |
            |   Hello   |   Adele   |