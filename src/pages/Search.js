const I = actor();

module.exports = {

  // Locators
  fields: {
    search: '//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input'
  },

  buttons: {
    search: '//*[@id="tsf"]/div[2]/div[1]/div[3]/center/input[1]'
  },

  // Functions
  search(text) {
    I.fillField(this.fields.search, text)
    I.click(this.buttons.search)
  }
}
