// Pages
const SearchPage = require('../pages/Search')

// CodeceptJS
const { I } = inject();

// Steps
Given('I navigate to search page', () => {
  I.amOnPage('/');
});

When('I search for the {string}', searchText => {
  SearchPage.search(searchText);
});

Then("I should see results with {string}", resultText => {
  I.see(resultText)
});
